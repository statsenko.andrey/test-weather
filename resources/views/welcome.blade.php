<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TEst</title>


    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script
            src="https://code.jquery.com/jquery-3.7.0.min.js"
            integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g="
            crossorigin="anonymous">
        
    </script>
    <script src="{{asset('js/my.js')}}"></script>
    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
</head>
<body class="antialiased">


<center>
    <h4>Сервис данных о погоде в г.Москва</h4>
    <br><br><br>
{{--    <button type="button" id="send" class="btn btn-primary col-md-4">Обновить данные о погоде</button>--}}
</center>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Дата</th>
        <th scope="col">Температура</th>
        <th scope="col">Облачность</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $dat )
        <tr>
            <td>{{$dat->date}}</td>
            <td>{{$dat->temp}}&deg;</td>
            <td>{{$dat->temp}}%</td>
        </tr>
    @endforeach
    </tbody>
</table>


</body>
</html>
