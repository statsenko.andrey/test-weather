<?php

namespace App\Http\Controllers;

use App\Models\Weather;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use SebastianBergmann\Diff\Exception;
use Illuminate\Http\Client\ConnectionException as ConnectionException;

class WeatherController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    
    public function index()
    {
        /**
         * По ТЗ нужно получить, сохранить и отоброазить данные из таблицы.
         *
         * Можно тут наворотить, кнопок на вьюшке наделать, типа обновить данные, и только тогда тянуть данные по апи и т.д.
         * но, на это времени нет )    
         * 
         *  Да, и бесплатный токен, работает только на 3-х часовых интервалах, поэтому я гружу всего на 8интервало вперед.
         */

        $url = 'http://api.openweathermap.org/data/2.5/forecast?lat=55.7522&lon=37.6156&&cnt=8&units=metric&lang=ru&appid=';
        $apiKey = '90f1fef900cc10582e6f6f0ac0bf917a'; //по хорошему это бы вынести в .env файл и оттуда брать
        try {
            $response = Http::get($url . $apiKey);
        } catch (ConnectionException $e) {
            dd($e->getMessage());
        }
        if ($response && $response->status() == 200) {
            $data = $response->object();

            $city = $data->city->name;
            //очистим от старых записей таблицу
            Weather::truncate();
            //запишем свежие данные
            foreach ($data->list as $row) {
                $temp = $row->main->temp;
                $clouds = $row->clouds->all;
                $date = $row->dt_txt;

                //запишем новые значения
                $weather = new Weather();
                $weather->city = $city;
                $weather->date = $date;
                $weather->temp = $temp;
                $weather->cloud = $clouds;
                $weather->save();
            }
        }

        //отобразим из таблицы на вьюшке
        $weather = Weather::all();
        return view('welcome')->with('data', $weather);;
    }

    
}
